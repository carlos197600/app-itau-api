package com.itau.eureka.client.controller;


import com.itau.eureka.client.service.ItauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/itau")
public class ItauController {

    @Autowired
    private ItauService service;

    @GetMapping
    public String findItau() {

        return service.verificarTransacoesEmBlackList();
    }


}