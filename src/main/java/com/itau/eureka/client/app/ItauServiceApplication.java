package com.itau.eureka.client.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.itau.eureka.client.controller","com.itau.eureka.client.service"})
public class ItauServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(ItauServiceApplication.class, args);
	}

}
