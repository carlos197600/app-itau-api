package com.itau.eureka.client.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class ItauService {

    private final List<String>  blackList = Arrays.asList("Transacao supeita por lavagem",
            "Transacao supeita por evasao de divisas", "Transacao supeita - fora do pradrão ");

    public String verificarTransacoesEmBlackList() {
        Random random = new Random();
        int randomIndex = random.nextInt(blackList.size());

        return blackList.get(randomIndex);

    }

}
